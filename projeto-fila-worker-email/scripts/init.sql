create database email_sender;
-- Conecta a base email_sender
\c email_sender

--ALTER USER postgres PASSWORD 'postgres';

create table emails (
	id serial not null,
	data timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	assunto varchar(100) not null,
	mensagem varchar(250) not null
);